import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import moment from 'moment';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
  return (
    <div className='ui container comments'>
      <ApprovalCard>
        <div className="ui statistic">
          <div className="value">{ faker.random.number() }</div>
          <div className="label">{ faker.random.word() }</div>
        </div>
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={ faker.name.firstName() }
          avatar={ faker.image.avatar() }
          text={ faker.lorem.sentence() }
          timeAgo={ moment(faker.date.recent()).fromNow() }/>
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={ faker.name.firstName() }
          avatar={ faker.image.avatar() }
          text={ faker.lorem.sentence() }
          timeAgo={ moment(faker.date.recent()).fromNow() } />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={ faker.name.firstName() }
          avatar={ faker.image.avatar() }
          text={ faker.lorem.sentence() }
          timeAgo={ moment(faker.date.past()).fromNow() } />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={ faker.name.firstName() }
          avatar={ faker.image.avatar() }
          text={ faker.lorem.sentence() }
          timeAgo={ moment(faker.date.past()).fromNow() } />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={ faker.name.firstName() }
          avatar={ faker.image.avatar() }
          text={ faker.lorem.sentence() }
          timeAgo={ moment(faker.date.past()).fromNow() } />
      </ApprovalCard>
    </div>
  );
};

ReactDOM.render(<App />, document.querySelector('#root'));

